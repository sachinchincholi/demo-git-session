@RestResource(urlmapping='/Mobile_FetchObjectsAndFields/*')
global with sharing class Mobile_FetchObjectsAndFields {
        
        public class APIParams implements IResponse{
            public list<String> soupNames;
        }
        
        @HttpPost
        global static list<IResponse> getSelectedObjects(){
         String rawParams = RestContext.request.requestBody.toString();
         System.debug('rawParams '+rawParams );
         APIParams p = (APIParams) JSON.deserialize(rawParams, APIParams.class);    
         System.Debug('***p***'+p);
         System.Debug('***p***'+p.soupNames);   
         return getSoupHelperMethod(p.soupNames);
         //return new list<APIParams>{p};
        }
        
        
        @HttpGet
        global static list<IResponse> getAllObjects(){
        RestRequest req = RestContext.request;
        String isSoupOnly = req.params.get('isSoupOnly');
        
        //ResponseSoupNames response;
        //ResponseToMobile wrap; 
        
        if(isSoupOnly=='true'){
            
            ResponseSoupNames response =  new ResponseSoupNames();
            
            List<Mobile_Objects_Settings__c> mcs = Mobile_Objects_Settings__c.getall().values();
            List<String> allSoupNames_New = new List<String>();
            
            for(Mobile_Objects_Settings__c mc : mcs){
                allSoupNames_New.add(mc.Object_API__c);
            }
            response.allSoupNames = allSoupNames_New;
            
            return new list<ResponseSoupNames>{response};
        }

        else{
            
            Set<String> standardFields = new Set<String>{'OwnerId', 'LastModifiedDate', 'SystemModstamp', 'LastReferencedDate', 'LastModifiedById', 'LastViewedDate', 'CreatedById', 'CreatedDate', 'IsDeleted'};
            
            try {
            //------List variables of 
            List<ResponseToMobile> objects = new List<ResponseToMobile>();
            Map<String, Set<String>> mapOfFields = new Map<String, Set<String>>();
            Map<String, Boolean> objectWithPushedStatus = new Map<String, Boolean>();
            List<Mobile_Objects_Settings__c> mobileSettingsList= new List<Mobile_Objects_Settings__c>();
            mobileSettingsList = [select id,Name,Object_API__c,Push_Records_to_Mobile__c from Mobile_Objects_Settings__c order by createdDate];
           
            //-----Loop through the custom setting Mobile_Setting records
            for(Mobile_Objects_Settings__c mobileSettings : mobileSettingsList) {
                
                //----Add default fields to the map         
                mapOfFields.put(mobileSettings.Object_API__c, new Set<String>{'Id', 'IsDirty'});
                objectWithPushedStatus.put(mobileSettings.Object_API__c, mobileSettings.Push_Records_to_Mobile__c);
                
                //----Loop through the fields of the Object 
                for(Schema.SObjectField fieldMetaData : Schema.getGlobalDescribe().get(mobileSettings.Object_API__c).getDescribe().fields.getMap().values()){
                    
                    //----Get describe of the fields to validate
                    Schema.DescribeFieldResult field = fieldMetaData.getDescribe();            
                        
                    //----- Fields which can be modified by the user are fetched
                    if(field.isAccessible() && !standardFields.contains(field.getName())) {
                            
                        //-----Populating map of object and set of fields
                        if(mapOfFields.containsKey(mobileSettings.Object_API__c)) {
                            mapOfFields.get(mobileSettings.Object_API__c).add(field.getName());
                        } else {
                            mapOfFields.put(mobileSettings.Object_API__c, new Set<String>{field.getName()});
                        }        
                    }   
                }
            }
            
            List<User_Time_Stamp__c> listOfCStoUpdate = new List<User_Time_Stamp__c>();
            List<User_Time_Stamp__c> listOfCStoInsert = new List<User_Time_Stamp__c>();
           
            
            // Find all the countries in the custom setting
            Map<String, User_Time_Stamp__c> lastRequestedByRecords = User_Time_Stamp__c.getAll();
            
            User_Time_Stamp__c lastUser = new User_Time_Stamp__c();
            for(User_Time_Stamp__c lU : lastRequestedByRecords.values()) {
                
                if(lU.Last_Logged_in_User__c == userInfo.getUserId()) {
                    lastUser = lU;
                }
            }
            
            //------Loop through the map of fields to get records
            for(Mobile_Objects_Settings__c mobileSettings : mobileSettingsList) {
            string objectFromList = mobileSettings.Object_API__c;
                //----Instance of the wrapper class
                ResponseToMobile wrp = new ResponseToMobile(objectFromList);
            
                //---- Object Name
                wrp.ObjectName = objectFromList;
                
               //-----Loop through the fields
                for(String fld : mapOfFields.get(objectFromList)) {                   
                    if(fld != 'IsDirty') {                
                        //Initiallize the wrapper class
                        //PathWithType pathwithT = new PathWithType();
                        //pathwithT.path = fld;
                        wrp.Fields.add(fld);                       
                    }
                }                           
                objects.add(wrp);                   
            }
            
            //Create record of Custom setting
            User_Time_Stamp__c updateInserted = new User_Time_Stamp__c();            
            if(lastUser.Id != null) {
                updateInserted.Id = lastUser.Id;
            }           
            updateInserted.Name = userInfo.getName();
            updateInserted.Last_Logged_in_User__c = userInfo.getUserId();
            updateInserted.Last_Modified_Date__c = System.now();           
            upsert updateInserted;            
            return objects;
        } catch(Exception e) {
            System.debug( '---ERROR Occured---' + e);
            return null;
        }   
        }
        return null;    
    }
    
    public static List<ResponseToMobile> getSoupHelperMethod(list<String> soupNames){
        Set<String> standardFields = new Set<String>{'OwnerId', 'LastModifiedDate', 'SystemModstamp', 'LastReferencedDate', 'LastModifiedById', 'LastViewedDate', 'CreatedById', 'CreatedDate', 'IsDeleted'};
            try {
            //------List variables of 
            List<ResponseToMobile> objects = new List<ResponseToMobile>();
            Map<String, Set<String>> mapOfFields = new Map<String, Set<String>>();
            Map<String, Boolean> objectWithPushedStatus = new Map<String, Boolean>();
            List<Mobile_Objects_Settings__c> mobileSettingsList= new List<Mobile_Objects_Settings__c>();
            if(!soupNames.isEmpty() || soupNames!=null){
                 mobileSettingsList = [select id,Name,Object_API__c,Push_Records_to_Mobile__c from Mobile_Objects_Settings__c where Object_API__c IN:soupNames order by createdDate];
            }else{
                 mobileSettingsList = [select id,Name,Object_API__c,Push_Records_to_Mobile__c from Mobile_Objects_Settings__c order by createdDate];
            }
            //-----Loop through the custom setting Mobile_Setting records
            for(Mobile_Objects_Settings__c mobileSettings : mobileSettingsList) {
                
                //----Add default fields to the map         
                mapOfFields.put(mobileSettings.Object_API__c, new Set<String>{'Id', 'IsDirty'});
                objectWithPushedStatus.put(mobileSettings.Object_API__c, mobileSettings.Push_Records_to_Mobile__c);
                
                //----Loop through the fields of the Object 
                for(Schema.SObjectField fieldMetaData : Schema.getGlobalDescribe().get(mobileSettings.Object_API__c).getDescribe().fields.getMap().values()){
                    
                    //----Get describe of the fields to validate
                    Schema.DescribeFieldResult field = fieldMetaData.getDescribe();            
                        
                    //----- Fields which can be modified by the user are fetched
                    if(field.isAccessible() && !standardFields.contains(field.getName())) {
                            
                        //-----Populating map of object and set of fields
                        if(mapOfFields.containsKey(mobileSettings.Object_API__c)) {
                            mapOfFields.get(mobileSettings.Object_API__c).add(field.getName());
                        } else {
                            mapOfFields.put(mobileSettings.Object_API__c, new Set<String>{field.getName()});
                        }        
                    }   
                }
            }
            
            List<User_Time_Stamp__c> listOfCStoUpdate = new List<User_Time_Stamp__c>();
            List<User_Time_Stamp__c> listOfCStoInsert = new List<User_Time_Stamp__c>();
           
            
            // Find all the countries in the custom setting
            Map<String, User_Time_Stamp__c> lastRequestedByRecords = User_Time_Stamp__c.getAll();
            
            User_Time_Stamp__c lastUser = new User_Time_Stamp__c();
            for(User_Time_Stamp__c lU : lastRequestedByRecords.values()) {
                
                if(lU.Last_Logged_in_User__c == userInfo.getUserId()) {
                    lastUser = lU;
                }
            }
            
            //------Loop through the map of fields to get records
            for(Mobile_Objects_Settings__c mobileSettings : mobileSettingsList) {
            string objectFromList = mobileSettings.Object_API__c;
                //----Instance of the wrapper class
                ResponseToMobile wrp = new ResponseToMobile(objectFromList);
            
                //---- Object Name
                wrp.ObjectName = objectFromList;
                
               //-----Loop through the fields
                for(String fld : mapOfFields.get(objectFromList)) {                   
                    if(fld != 'IsDirty') {                
                        //Initiallize the wrapper class
                        //PathWithType pathwithT = new PathWithType();
                        //pathwithT.path = fld;
                        wrp.Fields.add(fld);                       
                    }
                }                           
                objects.add(wrp);                   
            }
            
            //Create record of Custom setting
            User_Time_Stamp__c updateInserted = new User_Time_Stamp__c();            
            if(lastUser.Id != null) {
                updateInserted.Id = lastUser.Id;
            }           
            updateInserted.Name = userInfo.getName();
            updateInserted.Last_Logged_in_User__c = userInfo.getUserId();
            updateInserted.Last_Modified_Date__c = System.now();           
            upsert updateInserted;            
            return objects;
        } catch(Exception e) {
            System.debug( '---ERROR Occured---' + e);
            return null;
        }
        
        
    } 
}