global with sharing class BatchCreateMetadataSoap_Unique implements Database.Batchable<String>,Database.AllowsCallouts{
    
    public MetadataService.MetadataPort service;
    List<MetadataService.Metadata> metaDataList = new List<MetadataService.Metadata>();
    Map<String,List<String>> ObjectNameFieldsListMap =  new Map<String,List<String>>();
    
    public BatchCreateMetadataSoap_Unique(Map<String,List<String>> ObjectNameFieldsListMap,MetadataService.MetadataPort service){
        this.ObjectNameFieldsListMap = ObjectNameFieldsListMap;
        this.service = service;
    }
    
    global Iterable<String> start(Database.BatchableContext BC)
    {
        return new List<String> { 'Do something', 'Do something else', 'And something more' };
    }
    
    global void execute(Database.BatchableContext BC, List<String> scope)
    {
        for(String objectName:ObjectNameFieldsListMap.keyset()){
            for(String str:ObjectNameFieldsListMap.get(objectName)){
            MetadataService.CustomField customField1 = new MetadataService.CustomField();
            customField1.fullName = objectName+'.'+str.replace(' ','_').trim()+'__c';
            customField1.label = str;
            customField1.type_x = 'Text';
            customField1.length = 42;
            metaDataList.add((MetadataService.Metadata)customField1);
            }
        }
        //MetadataService.MetadataPort service = MetadataServicesCreation.createService();
        List<MetadataService.SaveResult> results = service.createMetadata(metaDataList); 
        
        System.debug('----------Meta data List---------------'+metaDataList);
    }
       
    global void finish(Database.BatchableContext BC)
    {

    
    }
    
    

}