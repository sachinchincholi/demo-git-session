global with sharing class ResponseToMobile implements IResponse{
		public string objectName;
		public list<String> fields = new list<String>();
		public integer size=0;
		
		global ResponseToMobile(String ObjectName){
			String Query = 'SELECT count() from '+ObjectName;
			size = Integer.valueOf(Database.CountQuery(Query));
		}
}