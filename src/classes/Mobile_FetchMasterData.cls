@RestResource(urlmapping='/Mobile_FetchMasterData/*')
global with sharing class Mobile_FetchMasterData {

	@Httpget
	global static list<respToMobile> getMasterData(){

		RestRequest req = RestContext.request;
		Integer offSetValue;
		Integer limitByValue;
		String soupName;
        list<sObject> resultList = new list<sObject>();

        list<respToMobile> respList = new list<respToMobile>();

        if(!String.isEmpty(req.params.get('index'))){
        	 offSetValue = Integer.valueOf(req.params.get('index'));
        }
        if(!String.isEmpty(req.params.get('limit'))){
        	 limitByValue = Integer.valueof(req.params.get('limit'));
        }
         if(!String.isEmpty(req.params.get('soupName'))){
        	 soupName  = req.params.get('soupName');
        }

        System.Debug(soupName+'-'+offSetValue+'-'+limitByValue);
        
        //String str  = Mobile_Utility.buildDynamicQueryWithOffsetandLimit('Account',1,10);
        String str = Mobile_Utility.buildDynamicQueryWithOffsetandLimit(soupName,offsetValue,limitByValue);
        
        resultList = Database.Query(str);

        respToMobile resp = new respToMobile();
        resp.soupName = soupName;
        resp.noOfRows = resultList.size();
        resp.result = resultList;
        respList.add(resp);

        return respList;
	}

	global class respToMobile{

		global String soupName;
		global Integer noOfRows;
		global list<sObject> result;

		global respToMobile(){
			result = new list<sObject>();
		}

	}


}