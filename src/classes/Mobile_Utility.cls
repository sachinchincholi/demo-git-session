public with sharing class Mobile_Utility {
	
	Private static final String customIndexFieldName = 'Integer_Custom_Index__c';


	public static String buildDynamicQuery(String objectName){
		String fieldSetName;

		List<Schema.FieldSetMember> allFields = new List<Schema.FieldSetMember>();

		fieldSetName = getFieldSetName(objectName);
		
		String query = 'SELECT ';

        allFields = readFieldSet(fieldSetName,objectName);

                //----- Get Fields from Field Set
        for(Schema.FieldSetMember field : allFields) {
          query += field.getFieldPath()+',';
		}

		query = query.removeEnd(',');

		query += ' FROM '+objectName;

		System.Debug('Query without Offset and Limit'+query);

		return query;
}
	/*
    * Generic method to get field set names
    */
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName,String ObjectName)
  {     
      System.debug('fieldSetName'+fieldSetName);
      System.debug('ObjectName'+ObjectName);
      
      Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 

      Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);     
      Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe(); 

      Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);

      //List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields(); 
      return fieldSetObj.getFields();
  } 

   public static String getFieldSetName(String objectName){
   		//list<String> listOfFieldSets  = new list<String>();
   		String fieldSetName;
   		List<Mobile_Objects_Settings__c> mcs = Mobile_Objects_Settings__c.getall().values();

   		for(Mobile_Objects_Settings__c eachCSRecord : mcs){
   			if(eachCSRecord.Object_API__c == objectName){
   				//listOfFieldSets = (eachCSRecord.Field_Set_Name__c.split(','));
   				fieldSetName = eachCSRecord.Field_Set_Name__c;
   			}
   		}
   		System.Debug(fieldSetName);

   		return fieldSetName;
   }

   public static String buildDynamicQueryWithOffsetandLimit(String objectName,Integer offsetValue,Integer limitValue){

   	String dynamicQuery = buildDynamicQuery(objectName);
   	System.Debug('***Dynamic Query without offset and limit'+dynamicQuery);

   	dynamicQuery += ' Where '+customIndexFieldName+'>='+offsetValue+' ORDER BY '+customIndexFieldName+' LIMIT '+limitValue;

   	system.debug('Final Query'+dynamicQuery);

   	return dynamicQuery;
   }

}