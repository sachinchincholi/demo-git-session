@RestResource(urlmapping='/MultiCalloutTest/*')
global with sharing class MultiCalloutTest {

  @Httpget
  global static void getMasterData(){
  
  
  
  HttpRequest reqData = new HttpRequest();
 String responseFromNet;
        Http http = new Http();
            
        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');
        reqData.setTimeout(20000);
      
        reqData.setEndpoint(' http://ip.jsontest.com/');
        reqData.setBody('Test');
        reqData.setMethod('POST');
        
        RestContext.response.addHeader('Content-Type', 'application/json');
          try {
            HTTPResponse res = http.send(reqData);
            responseFromNet = res.getBody();
            insert new Contact(lastName='Test');
            system.debug('--responseFromNet--'+responseFromNet);
          
          }
          catch(Exception e){
              system.assertEquals(1,2);
          }
          
         RestContext.response.responseBody =  Blob.valueOf(responseFromNet);
  }
  }